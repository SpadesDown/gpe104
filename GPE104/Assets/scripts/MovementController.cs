﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{

    public float speed = 1f;

    // Update is called once per frame
    void Update()
    {
        //this checks to see if the player is holding space and if so then resets the player object to the center of the screen else allows the player to move
        if(!Input.GetKey(KeyCode.Space))
        {
            //this allows for controll of the player object using the arrow keys on the keyboard
            //moves player object up
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                transform.position += new Vector3(0, speed, 0);
            }

            //moves player object down
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                transform.position += new Vector3(0, -speed, 0);
            }

            //moves player object right
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                transform.position += new Vector3(speed, 0, 0);
            }

            //moves player object left
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                transform.position += new Vector3(-speed, 0, 0);
            }
        }
        else
        {
            transform.position = Vector3.zero;
        }
    }
}
