﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //this is the player game object thats in the scene
    public GameObject Player;

    // Update is called once per frame
    void Update()
    {
        //this disables and enables the player object 
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Player.SetActive(!Player.activeSelf);
        }

        //when the escape key is pressed then the game exits
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
