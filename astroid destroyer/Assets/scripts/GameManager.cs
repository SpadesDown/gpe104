﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    private int playerLives = 3;
    private int playerScore = 0;

    [SerializeField]
    private GameObject[] astroids;
    [SerializeField]
    private Transform playerPrefab;
    [SerializeField]
    private float astroidSpeed = 1f;
    [SerializeField]
    private float astroidSpawnRate = 1f;
    [SerializeField]
    private Image lifeImage;
    [SerializeField]
    private Transform lifeImageParent;
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text livesText;
    [SerializeField]
    private GameObject SpawnInfo;
    [SerializeField]
    private GameObject gameOverMenu;

    private float nextSpawn;

    private bool canSpawn = true;
    private bool gameOver = false;

    // Start is called before the first frame update
    void Awake()
    {
        SpawnInfo.SetActive(false);
        gameOverMenu.SetActive(false);
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.Log("GameManager instance already exists");
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextSpawn && canSpawn)
        {
            SpawnAstroid();
            nextSpawn = Time.time + 1f / astroidSpawnRate;
        }

        if(!canSpawn && !gameOver && Input.GetKeyDown(KeyCode.Space))
        {
            SpawnPlayer();
        }
    }

    public void AddLife()
    {
        playerLives += 1;
        livesText.text = " Lives " + playerLives.ToString();
    }

    public void TakeLife()
    {
        
        playerLives -= 1;
        livesText.text = " Lives " + playerLives.ToString();
        foreach (asteroid item in FindObjectsOfType<asteroid>())
        {
            Destroy(item.gameObject);
        }
        if (playerLives > 0)
        {
            playerPrefab.transform.position = Vector3.zero;
            playerPrefab.gameObject.SetActive(false);
            canSpawn = false;
            SpawnInfo.SetActive(true);
        }
        else
        {
            playerPrefab.transform.position = Vector3.zero;
            playerPrefab.gameObject.SetActive(false);
            canSpawn = false;
            GameOver();
        }
    }

    public void AddScore(int _amt)
    {
        playerScore += _amt;
        scoreText.text = "Score " + playerScore.ToString();
    }

    void SpawnPlayer()
    {
        playerPrefab.gameObject.SetActive(true);
        canSpawn = true;
        SpawnInfo.SetActive(false);
    }

    void GameOver()
    {
        canSpawn = false;
        gameOver = true;
        gameOverMenu.SetActive(true);
    }

    public void Restart()
    {
        playerScore = 0;
        playerLives = 3;
        livesText.text = "Lives " + playerLives.ToString();
        scoreText.text = "Score " + playerScore.ToString();
        SpawnPlayer();
        gameOver = false;
        gameOverMenu.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void SpawnAstroid()
    {
        Vector3 center = playerPrefab.position;

        Vector3 pos = RandomCircle(center, 8);

        int astroid = Random.Range(0, astroids.Length);

        Quaternion rot = Quaternion.LookRotation(Vector3.forward, center - pos);
        GameObject currentAstroid = Instantiate(astroids[astroid], pos, rot);

        currentAstroid.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * astroidSpeed);

        Destroy(currentAstroid, 4f);
    }

    Vector3 RandomCircle(Vector3 center, float radius)
    {
        float ang = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
}
