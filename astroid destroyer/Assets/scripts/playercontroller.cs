﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playercontroller : MonoBehaviour
{
    [SerializeField]
    private float speed = 1f;
    [SerializeField]
    private float rotationSpeed = 1f;
    [SerializeField]
    private float fireRate = 1f;
    [SerializeField]
    private float bulletSpeed = 1f;
    [SerializeField]
    private GameObject bulletPrefab;
    [SerializeField]
    private Transform shotSpawn;
    [SerializeField]
    private Boundary boundary;

    private Rigidbody2D rigidbody;

    private float nextFire = 0;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.RightArrow))
            transform.Rotate(new Vector3(0, 0, -1) * Time.deltaTime * rotationSpeed);

        if (Input.GetKey(KeyCode.LeftArrow))
            transform.Rotate(new Vector3(0, 0, 1) * Time.deltaTime * rotationSpeed);

        if (Input.GetKey(KeyCode.UpArrow))
            rigidbody.AddRelativeForce(new Vector2(0, 1) * Time.deltaTime * speed);

        if (Input.GetKey(KeyCode.DownArrow))
            rigidbody.AddRelativeForce(new Vector2(0, -1) * Time.deltaTime * speed);

        if (Input.GetKey(KeyCode.Space) && Time.time >= nextFire)
        {
            Fire();
        }

        rigidbody.position = new Vector3(Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax), Mathf.Clamp(rigidbody.position.y, boundary.yMin, boundary.yMax));

    }

    public void Fire()
    {
        nextFire = Time.time + 1f/fireRate;
        GameObject currentBullet = Instantiate(bulletPrefab, shotSpawn.position, shotSpawn.rotation);

        currentBullet.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0, 1) * Time.deltaTime * bulletSpeed);

        Destroy(currentBullet, 1f);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.tag == "asteroid")
        {
            GameManager.instance.TakeLife();
        }
    }
}

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, yMin, yMax;
}
