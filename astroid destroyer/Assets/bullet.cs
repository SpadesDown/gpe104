﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        if(other.transform.tag == "asteroid")
        {
            GameManager.instance.AddScore(10);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
